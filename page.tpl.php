<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <title>
      <?php print $head_title; ?>
    </title>
    <?php print $head; ?>
    <?php print $styles; ?>
    <?php print $scripts; ?>
<!--[if lte IE 6]>
<link href="<?php print $base_path . $directory; ?>/css/template_ie.css" rel="stylesheet" type="text/css" />
<![endif]-->

  </head>
  <body id="page_bg">
  <div id="header">
            <?php if ($logo): ?>
              <a href="<?php print $base_path; ?>" title="<?php print t('Home'); ?>">
              <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" id="logo" />
              </a>
            <?php endif; ?>
			  <?php if ($site_name) { ?><div id="siteName"><a href="<?php print $base_path ?>" title="<?php print t('Home') ?>"><?php print $site_name ?></a></div><?php } ?>
            <div id="top">
              <div class="padding">
                      <?php if ($header): ?>
                        <div id="header_block" class="header">
                          <?php print $header; ?>
                        </div> <!-- /header -->
                      <?php endif; ?>                      
              </div>
            </div>
      </div>
	  <div id="horiz-menu">
        <div id="navigation" class="menu">
				  <?php if (isset($primary_links)) : ?>      
            <div id="primary" class="clear-block">
              <?php print theme('links', $primary_links, array('class' => 'links primary-links')) ?>
            </div>
          <?php endif; ?>
            
          <?php if ($secondary_links): ?>
            <div id="secondary" class="clear-block">
              <?php print theme('links', $secondary_links, array('class' => 'links secondary-links')) ?>
            </div>
          <?php endif; ?>
        </div> <!-- /navigation -->      
      </div>
    <div id="wrapper">
      
      
      <div id="mainbody">
   
              <div class="padding">

                      <div>

							
                              <div id="main" class="column"><div id="squeeze">
				<div id="contentblock">
                      <?php if ($top_left): ?>
                        <div id="top_left" class="column sidebar">
                          <?php print $top_left; ?>
                        </div> <!-- /top_left -->
                      <?php endif; ?>  


                      <?php if ($top_right): ?>
                        <div id="top_right" class="column sidebar">
                          <?php print $top_right; ?>
                        </div> <!-- /top_right -->
                      <?php endif; ?> 
                </div>
  
							                <?php if ($breadcrumb): ?>
                  <div id="bread">
                    <?php print $breadcrumb; ?>
                  </div>
                <?php endif; ?>  
							  
                                <?php if ($content_top):?><div id="content-top"><?php print $content_top; ?></div><?php endif; ?>
                                <?php if ($title): ?><h1 class="title"><?php print $title; ?></h1><?php endif; ?>
                                <?php if ($tabs): ?><div class="tabs"><?php print $tabs; ?></div><?php endif; ?>
                                <?php print $help; ?>
                                <?php print $messages; ?>
                                <?php print $content; ?>
                                <?php print $feed_icons; ?>
                                <?php if ($content_bottom): ?><div id="content-bottom"><?php print $content_bottom; ?></div><?php endif; ?>
                              </div></div> <!-- /squeeze /main -->
                      </div>
              </div>

              <div class="padding">
                <div class="moduletable">
                  <?php if ($left || $right): ?>
                    <div id="sidebar-right" class="column sidebar">
                      <?php if ($left) {  print $left; } ?>
                      <?php if ($right) { print $right; } ?>
                    </div> <!-- /sidebar-right -->
                  <?php endif; ?>
                </div>
              </div>


      <div id="bottom">
       
<div class="node">

                <?php if ($bottom_left): ?>
                  <div id="bottom_left" class="column sidebar">
                    <?php print $bottom_left; ?>
                  </div> <!-- /bottom_left -->
                <?php endif; ?>



                <?php if ($bottom_right): ?>
                  <div id="bottom_right" class="column sidebar">
                    <?php print $bottom_right; ?>
                  </div> <!-- /bottom_right -->
                <?php endif; ?>
</div>
      </div>
<div id="footer">
				<p>Copyright infomation comes here Theme by <a href="http://drupal.org/user/198692">Rishikesh</a></p>
		
		</div>
         </div> <!-- 1189991641 -->
  <?php print $closure ?>
  </body>
</html>
